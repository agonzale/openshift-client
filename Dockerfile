FROM gitlab-registry.cern.ch/paas-tools/openshift-client

COPY tmux.conf /root/.tmux.conf

# Install tools to be able to extract and use the oc client
RUN yum install -y tar vim tmux less && yum clean all && \
    echo '. .bash_profile' >>/root/.bashrc && \
    echo 'set bg=dark' >>/root/.vimrc && \
    echo 'export PS1="\[\e[1;34m\]\u\[\e[0m\]@\[\e[32m\]\$\(oc project -q\) \[\e[1;34m\]\W \[\e[0m\]\$ "' >>/root/.bash_profile

